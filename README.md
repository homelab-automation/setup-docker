# Setup Docker

Configure your system to use Docker and have target user be a member of the docker group.

## Requirements

This has only been tested on CentOS 7.

## Role Variables

|     variable     | default  |                    description                     |
| :--------------: | :------: | :------------------------------------------------: |
| `docker_version` | "latest" |     The version of docker you want to install.     |
|      `user`      | "setme"  | The user you want to be added to the docker group. |

## Example Playbook

Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too:

    - hosts: servers
      roles:
         - { role: username.rolename, user: myuser }
